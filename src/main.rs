mod db;
mod scrapper;

use db::*;
use scrapper::{fetch_auction_data};
use rusqlite::{Connection, Result};
use std::env;
use regex::Regex;
use serde_json::Value;

pub fn parse_html(html: &str) {
    let re_script = Regex::new(r"<script>(.*?)</script>").unwrap();
    let scripts: Vec<_> = re_script.captures_iter(html).collect();
    let script_content = &scripts[9][1];

    let re_events_data = Regex::new(r"\"events\":(\[.*?\]),").unwrap();
    let events_data = re_events_data.captures(&script_content).unwrap().get(1).unwrap().as_str();

    let events: Vec<Value> = serde_json::from_str(events_data).unwrap();

    for event in events {
        let event_id = event["event_id"].as_i32().unwrap();
        let landing_url = event["landing_url"].as_str().unwrap();
        let title = event["title_txt"].as_str().unwrap();
        let subtitle = event["subtitle_txt"].as_str().unwrap_or("");
        let performed_at = event["start_date"].as_str().unwrap();
        let analytics_id = event["analytics_id"].as_str().unwrap();
        let sale_total_txt = event["sale_total_txt"].as_str().unwrap_or("");
        let sale_total_val = event["sale_total_value_txt"].as_str().unwrap_or("");

        let new_event = db::Event {
            id: None,
            event_id,
            landing_url: String::from(landing_url),
            title: String::from(title),
            subtitle: String::from(subtitle),
            performed_at: String::from(performed_at),
            analytics_id: String::from(analytics_id),
            sale_total_txt: String::from(sale_total_txt),
            sale_total_val: String::from(sale_total_val),
        };

        let conn = db::connect_to_db().expect("Failed to connect to the database");
        db::insert_event(&conn, &new_event).expect("Failed to insert the event");
    }
}

fn fetch_auctions(year: String, month: String, conn: &Connection) {
    match fetch_auction_data(&year, &month) {
        Ok(html_content) => {
            let events = parse_html(&html_content);

            for event in events {
                // TODO Toto se teď vyrábí už v "parse_html"
                db::insert_event(&conn, &event).expect("Ok");
            }
        }
        Err(error) => {
            eprintln!("Error fetching auction data: {}", error);
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let db_file = env::var("SQLITE_FILE")
        .expect("SQLITE_FILE must be set");

    let conn = connect_to_db(db_file)?;
    create_tables(&conn)?;

    // Test the database connection and tables
    match test_db(&conn) {
        Ok(_) => println!("Database connection and tables verified."),
        Err(e) => {
            eprintln!("Error: {:?}", e);
            std::process::exit(1);
        }
    }

    // Test scrape some data
    fetch_auctions(String::from("2013"), String::from("6"), &conn);

    Ok(())
}