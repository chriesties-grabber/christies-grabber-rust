use rusqlite::{Connection, Result, params};

#[derive(Debug)]
pub struct Event {
    pub id: Option<i32>,
    pub event_id: i32,
    pub landing_url: String,
    pub title: String,
    pub subtitle: Option<String>,
    pub performed_at: Option<String>,
    pub analytics_id: String,
    pub sale_total_txt: Option<String>,
    pub sale_total_val: Option<String>,
}

#[derive(Debug)]
pub struct EventItem {
    pub id: Option<i32>,
    pub event_id: i32,
    pub object_id: String,
    pub url: Option<String>,
    pub title: String,
    pub subtitle: Option<String>,
    pub description: Option<String>,
    pub price_estimated_low: Option<String>,
    pub price_estimated_high: Option<String>,
    pub price_estimated: Option<String>,
    pub price_realised: Option<String>,
    pub price_realised_txt: Option<String>,
    pub lot_withdrawn: bool,
}

pub fn connect_to_db(key: String) -> Result<Connection> {
    Connection::open(key)
}

pub fn create_tables(conn: &Connection) -> Result<()> {
    conn.execute(
        "CREATE TABLE IF NOT EXISTS events (
            id INTEGER PRIMARY KEY,
            event_id INTEGER NOT NULL,
            landing_url TEXT NOT NULL,
            title TEXT NOT NULL,
            subtitle TEXT,
            performed_at DATETIME,
            analytics_id TEXT NOT NULL,
            sale_total_txt TEXT,
            sale_total_val TEXT
        )",
        (),
    )?;

    conn.execute(
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_events_event_id ON events (event_id);",
        (),
    )?;

    conn.execute(
        "CREATE INDEX IF NOT EXISTS idx_events_id ON events (event_id);",
        (),
    )?;

    conn.execute(
        "CREATE INDEX IF NOT EXISTS idx_events_title ON events (title);",
        (),
    )?;

    conn.execute(
        "CREATE INDEX IF NOT EXISTS idx_events_analytics_id ON events (analytics_id);",
        (),
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS event_items (
            id INTEGER PRIMARY KEY,
            event_id INTEGER NOT NULL,
            object_id TEXT NOT NULL,
            url TEXT,
            title TEXT NOT NULL,
            subtitle TEXT,
            description TEXT,
            price_estimated_low TEXT,
            price_estimated_high TEXT,
            price_estimated TEXT,
            price_realised TEXT,
            price_realised_txt TEXT,
            lot_withdrawn BOOLEAN,
            FOREIGN KEY (event_id) REFERENCES events (id)
        )",
        (),
    )?;

    conn.execute(
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_event_items_unique_event_object 
            ON event_items (event_id, object_id);",
        (),
    )?;

    Ok(())
}

pub fn test_db(conn: &Connection) -> Result<()> {
    // Test the database connection by executing a simple query
    conn.query_row("SELECT 1", [], |_| Ok(()))?;

    // Check if the "events" table exists
    let events_table_exists: bool = conn.query_row(
        "SELECT (count(*) > 0) FROM sqlite_master WHERE type='table' AND name='events'",
        [],
        |row| { row.get(0) })?;


    if events_table_exists != true {
        return Err(rusqlite::Error::QueryReturnedNoRows);
    }

    // Check if the "event_items" table exists
    let event_items_table_exists: bool = conn.query_row(
        "SELECT (count(*) > 0) FROM sqlite_master WHERE type='table' AND name='event_items'",
        [],
        |row| row.get(0),
    )?;

    if event_items_table_exists != true {
        return Err(rusqlite::Error::QueryReturnedNoRows);
    }

    Ok(())
}

pub fn insert_event(conn: &Connection, event: &Event) -> Result<()> {
    let sql = "
        INSERT OR IGNORE INTO events (
            event_id, landing_url, title, subtitle, performed_at, analytics_id, sale_total_txt, sale_total_val
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)
    ";
    conn.execute(
        sql,
        params![
            event.event_id,
            event.landing_url,
            event.title,
            event.subtitle,
            event.performed_at,
            event.analytics_id,
            event.sale_total_txt,
            event.sale_total_val,
        ],
    )?;

    Ok(())
}

pub fn insert_event_item(conn: &Connection, event_item: &EventItem) -> Result<()> {
    let sql = "
        INSERT OR IGNORE INTO event_items (
            event_id, object_id, url, title, subtitle, description, price_estimated_low, price_estimated_high, price_estimated, price_realised, price_realised_txt, lot_withdrawn
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ";
    conn.execute(
        sql,
        params![
            event_item.event_id,
            event_item.object_id,
            event_item.url,
            event_item.title,
            event_item.subtitle,
            event_item.description,
            event_item.price_estimated_low,
            event_item.price_estimated_high,
            event_item.price_estimated,
            event_item.price_realised,
            event_item.price_realised_txt,
            event_item.lot_withdrawn,
        ],
    )?;

    Ok(())
}

/**
 * ==========================================================================
 * Tests
 * ==========================================================================
 * 
 */


#[cfg(test)]
mod tests {
    use super::*;

    mod tests_common {
        use super::*;
    
        pub fn setup_db() -> Result<rusqlite::Connection> {
            let db_file = String::from(":memory:");
        
            let conn = connect_to_db(db_file)?;
            create_tables(&conn)?;
            Ok(conn)
        }
    }

    use tests_common::setup_db;

    #[test]
    fn test_create_tables_and_test_db() {
        let conn = setup_db().unwrap();
        let res: bool = match test_db(&conn) {
            Ok(_) => true,
            Err(_e) => false
        };

        assert!(res, "Database was not created correctly!");
    }

    #[test]
    fn test_insert_event_and_event_item() {
        let conn = setup_db().unwrap();

        // Test insert event
        let event = Event {
            id: None,
            event_id: 1,
            landing_url: "https://example.com/event/1".to_string(),
            title: "Test Event".to_string(),
            subtitle: Some("Test Event Subtitle".to_string()),
            performed_at: Some("2023-04-01T12:00:00Z".to_string()),
            analytics_id: "test_event_1".to_string(),
            sale_total_txt: Some("Estimated Sale Total".to_string()),
            sale_total_val: Some("12345".to_string()),
        };

        insert_event(&conn, &event).expect("Ok");

        // Test count of items
        let count: i32 = conn.query_row("SELECT count(id) FROM events", [], |row| row.get(0)).unwrap();
        assert_eq!(count, 1);

        // Test insert event item
        let event_item = EventItem {
            id: None,
            event_id: 1,
            object_id: "object_1".to_string(),
            url: Some("https://example.com/event/1/object/1".to_string()),
            title: "Test Object".to_string(),
            subtitle: Some("Test Object Subtitle".to_string()),
            description: Some("Test Object Description".to_string()),
            price_estimated_low: Some("1000".to_string()),
            price_estimated_high: Some("2000".to_string()),
            price_estimated: Some("1000-2000".to_string()),
            price_realised: Some("1500".to_string()),
            price_realised_txt: Some("Realised Price".to_string()),
            lot_withdrawn: false,
        };

        insert_event_item(&conn, &event_item).expect("Ok");

        // Test count of items
        let count: i32 = conn.query_row("SELECT count(id) FROM event_items", [], |row| row.get(0)).unwrap();
        assert_eq!(count, 1);
    }
}