use reqwest::blocking::Client;
use std::error::Error;

pub fn fetch_auction_data(year: &String, month: &String) -> Result<String, Box<dyn Error>> {
    let url = format!("https://www.christies.com/en/results?year={}&month={}", year.to_string(), month.to_string());
    let client = Client::new();

    let response = client.get(&url).send()?;

    if response.status().is_success() {
        let html_content = response.text()?;
        Ok(html_content)
    } else {
        Err(format!("Failed to fetch the page: {}", url).into())
    }
}