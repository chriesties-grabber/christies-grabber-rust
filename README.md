# Data scrapper from Christie's

__Still work in progress__

It collects all auctions and auction items since 1998 when the Christie's went online.

Friend of mine wanted to collect all these data and firstly I created simple PHP app to do it - just because I have no time - this is just a learning project (as well for the Rust as for ChaptGPT).

Since this program was made by using ChatGPT-4 - the whole [conversation](docs/chatgpt-conversation.md) is available. I didn't know Rust very well so I tried to learn with _AI_ - as you can see from commits and if you will compare them against the questions/answers - all code was rewritten - but still it was huge help to learn new things.